package com.clearos.clearnav.core.database.model;

import com.clearos.clearnav.core.utils.Constants;

public class ShortcutItem extends LauncherItem {

    public ShortcutItem(){
        itemType = Constants.ITEM_TYPE_SHORTCUT;
    }
}
