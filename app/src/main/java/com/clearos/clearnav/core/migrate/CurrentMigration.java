package com.clearos.clearnav.core.migrate;

import java.util.List;

public class CurrentMigration {
    public int currentVersion;
    public List<MigrationInfo> migrate_infos;
}
