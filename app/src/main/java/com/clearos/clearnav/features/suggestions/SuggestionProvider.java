package com.clearos.clearnav.features.suggestions;

import io.reactivex.Single;

public interface SuggestionProvider {

    Single<SuggestionsResult> query(String query);
}
