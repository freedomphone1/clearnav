package com.clearos.clearnav.features.launcher.tasks;

import android.os.AsyncTask;

import java.util.Map;

import com.clearos.clearnav.core.database.model.ApplicationItem;
import com.clearos.clearnav.core.utils.AppUtils;
import com.clearos.clearnav.features.launcher.AppProvider;

public class LoadAppsTask extends AsyncTask<Void, Void, Map<String, ApplicationItem>> {

    private AppProvider mAppProvider;

    public LoadAppsTask() {
        super();
    }

    public void setAppProvider(AppProvider appProvider) {
        this.mAppProvider = appProvider;
    }

    @Override
    protected Map<String, ApplicationItem> doInBackground(Void... voids) {
        return AppUtils.loadAll(mAppProvider.getContext());
    }

    @Override
    protected void onPostExecute(Map<String, ApplicationItem> appItemPair) {
        super.onPostExecute(appItemPair);
        if (mAppProvider != null) {
            mAppProvider.loadAppsOver(appItemPair);
        }
    }
}
