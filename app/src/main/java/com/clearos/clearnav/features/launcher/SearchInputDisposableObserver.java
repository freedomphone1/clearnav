package com.clearos.clearnav.features.launcher;

import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import com.clearos.clearnav.R;
import com.clearos.clearnav.activity.SplashActivity;
import com.clearos.clearnav.core.customviews.BlissFrameLayout;
import com.clearos.clearnav.core.database.model.LauncherItem;
import com.clearos.clearnav.features.suggestions.AutoCompleteAdapter;
import com.clearos.clearnav.features.suggestions.SuggestionsResult;
import io.reactivex.observers.DisposableObserver;

public class SearchInputDisposableObserver extends DisposableObserver<SuggestionsResult> {

    private AutoCompleteAdapter networkSuggestionAdapter;
    private SplashActivity splashActivity;
    private ViewGroup appSuggestionsViewGroup;

    public SearchInputDisposableObserver(SplashActivity activity, RecyclerView.Adapter autoCompleteAdapter, ViewGroup viewGroup) {
        this.networkSuggestionAdapter = (AutoCompleteAdapter) autoCompleteAdapter;
        this.splashActivity = activity;
        this.appSuggestionsViewGroup = viewGroup;
    }

    @Override
    public void onNext(SuggestionsResult suggestionsResults) {
        if (suggestionsResults.type
                == SuggestionsResult.TYPE_NETWORK_ITEM) {
            networkSuggestionAdapter.updateSuggestions(suggestionsResults.getNetworkItems(),
                    suggestionsResults.queryText);
        } else if (suggestionsResults.type
                == SuggestionsResult.TYPE_LAUNCHER_ITEM) {
            ((ViewGroup)appSuggestionsViewGroup.findViewById(R.id.suggestedAppGrid)).removeAllViews();
            appSuggestionsViewGroup.findViewById(R.id.openUsageAccessSettings).setVisibility(View.GONE);
            appSuggestionsViewGroup.findViewById(R.id.suggestedAppGrid).setVisibility(View.VISIBLE);
            for (LauncherItem launcherItem : suggestionsResults
                    .getLauncherItems()) {
                BlissFrameLayout blissFrameLayout = splashActivity.prepareSuggestedApp(
                        launcherItem);
                splashActivity.addAppToGrid(appSuggestionsViewGroup.findViewById(R.id.suggestedAppGrid), blissFrameLayout);
            }
        } else {
            splashActivity.refreshSuggestedApps(appSuggestionsViewGroup,true);
            networkSuggestionAdapter.updateSuggestions(new ArrayList<>(),
                    suggestionsResults.queryText);
        }
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public void onComplete() {
    }
}
