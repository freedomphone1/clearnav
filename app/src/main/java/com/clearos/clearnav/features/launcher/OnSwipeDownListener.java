package com.clearos.clearnav.features.launcher;

public interface OnSwipeDownListener {

    void onSwipeStart();
    void onSwipe(int position);
    void onSwipeFinish();
}
